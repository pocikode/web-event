<?php

namespace App\Http\Controllers;

use App\Models\Event;

class EventBookController extends Controller
{
    public function show($id)
    {
        $event = Event::findOrFail($id);

        return view('event-detail', compact('event'));
    }
}
