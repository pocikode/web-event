<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    public function book(Request $request)
    {
        $event = Event::findOrFail($request->event_id);

        $transactionNo = '0001';

        $lastTransaction = Transaction::query()
            ->whereDate('created_at', now())
            ->orderBy('id', 'desc')
            ->first();

        if ($lastTransaction) {
            $transactionNo = substr($lastTransaction->transaction_no, 10);
        }

        $transaction = new Transaction();
        $transaction->transaction_no = date('YmdH' . $transactionNo);
        $transaction->user_id = Auth::id();
        $transaction->event_id = $event->id;
        $transaction->price = $event->price;
        $transaction->qty = $request->qty;
        $transaction->save();

        return redirect()->route('transaction.show', $transaction->id);
    }

    public function show($id)
    {
        $transaction = Transaction::with('user', 'event')
            ->where('user_id', Auth::id())
            ->where('id', $id)
            ->firstOrFail();

        return view('transaction', compact('transaction'));
    }
}
