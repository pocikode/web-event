<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function create()
    {
        return view('payment-confirmation');
    }

    public function store(Request $request)
    {
        $filename = time() . '.' . $request->payment_receipt->extension();
        $image = $request->file('payment_receipt')->storeAs('payment', $filename, 'public');

        $payment = new Payment();
        $payment->transaction_no = $request->transaction_no;
        $payment->name = $request->name;
        $payment->email = $request->email;
        $payment->payment_date = Carbon::createFromFormat('m/d/Y', $request->payment_date);
        $payment->amount = $request->payment_amount;
        $payment->bank = $request->bank;
        $payment->sender_account_name = $request->sender_account_name;
        $payment->notes = $request->notes;
        $payment->receipt_image = $image;
        $payment->save();

        return redirect()
            ->route('payment.create')
            ->with(['success' => 'Success']);
    }
}
