<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\PaymentConfirmed;
use App\Models\Payment;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class PaymentController extends Controller
{
    public function index()
    {
        $payments = Payment::query()
            ->orderBy('id', 'desc')
            ->paginate(10);

        $statusColors = [
            Payment::STATUS_WAITING  => 'text-warning',
            Payment::STATUS_APPROVED => 'text-success',
            Payment::STATUS_REJECTED => 'text-danger',
        ];

        return view('admin.payment.index', compact('payments', 'statusColors'));
    }

    // Ajax Only
    public function show(Request $request, $id)
    {
        if ($request->wantsJson() || $request->ajax()) {
            $payment = Payment::with('transaction')->findOrFail($id);
            $payment->receipt_image_url = Storage::url($payment->receipt_image);

            return response()->json($payment);
        }

        return redirect()->route('dashboard.payment.index');
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();

        $status = $request->status;
        $payment = Payment::with('transaction')->findOrFail($id);

        if (!$payment->transaction && $status === 'approved') {
            return redirect()
                ->back()
                ->with('error', 'Transaction number is not valid');
        }

        $payment->status = $status;
        $payment->save();

        $transaction = $payment->transaction;

        if ($status === 'approved') {
            $transaction->status = Transaction::STATUS_PAID;

            if ($transaction->event->event_type === 'Offline') {
                $transaction->booking_code = substr(str_shuffle('1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 8);
            }

            $transaction->save();

            Mail::to($transaction->user)->send(new PaymentConfirmed($payment));
        }

        DB::commit();

        return redirect()
            ->route('dashboard.payment.index')
            ->with('success', "Payment with transaction number {$payment->transaction_no} has been {$status} successfully");
    }
}
