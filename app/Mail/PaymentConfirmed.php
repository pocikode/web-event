<?php

namespace App\Mail;

use App\Models\Payment;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PaymentConfirmed extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        private Payment $payment
    ) {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.payment-confirmed')
                    ->with([
                        'payment' => $this->payment,
                        'transaction' => $this->payment->transaction,
                        'event'   => $this->payment->transaction->event,
                        'user'    => $this->payment->transaction->user,
                    ]);
    }
}
