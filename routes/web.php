<?php

use App\Http\Controllers\Admin;
use App\Http\Controllers\EventBookController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\TransactionController;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//auth route for both

Route::group(['middleware' => ['auth']], function(){
    Route::get('/dashboard', 'App\Http\Controllers\DashboardController@index')->name
    ('dashboard');
});

//auth route for admin
Route::group([
    'middleware' => ['auth', 'role:admin'],
    'prefix' => 'dashboard',
    'as'     => 'dashboard.',
], function(){
    Route::get('content', 'App\Http\Controllers\DashboardController@content')->name('content');
    Route::post('content/store', 'App\Http\Controllers\DashboardController@storeContent')->name('store.content');
    Route::get('payment', 'App\Http\Controllers\DashboardController@payment')->name('payment');
    Route::get('addSlider', 'App\Http\Controllers\DashboardController@addSlider')->name('addSlider');
    Route::get('editSlider/{id}', 'App\Http\Controllers\DashboardController@editSlider')->name('editSlider');
    Route::put('updateSlider/{id}', 'App\Http\Controllers\DashboardController@updateSlider')->name('updateSlider');
    Route::post('content/{$id}', 'App\Http\Controllers\DashboardController@destroySlider')->name('destroySlider');

    Route::group(['prefix' => 'payment', 'as' => 'payment.'], function() {
        Route::get('/', [Admin\PaymentController::class, 'index'])->name('index');
        Route::get('/{id}', [Admin\PaymentController::class, 'show'])->name('show');
        Route::put('/{id}', [Admin\PaymentController::class, 'update'])->name('update');
    });
});

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

Route::get('/dashboard/create/event', 'App\Http\Controllers\EventController@create')->name
    ('dashboard.create.event');

Route::post('/dashboard/store/event', 'App\Http\Controllers\EventController@store')->name
('dashboard.store.event');

Route::get('/dashboard/event', 'App\Http\Controllers\EventController@index')->name
('dashboard.event');

Route::get('/dashboard/eventDetail/{$id}', 'App\Http\Controllers\EventController@eventDetail')->name
('dashboard.eventDetail');

Route::get('events/{id}', [EventBookController::class, 'show'])->name('events.show');

Route::post('transaction/book', [TransactionController::class, 'book'])->name('transaction.book');
Route::get('transaction/{id}', [TransactionController::class, 'show'])->name('transaction.show');

Route::get('payment', [PaymentController::class, 'create'])->name('payment.create');
Route::post('payment', [PaymentController::class, 'store'])->name('payment.confirm');

require __DIR__.'/auth.php';
