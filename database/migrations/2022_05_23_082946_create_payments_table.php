<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->string('transaction_no');
            $table->string('name');
            $table->string('email');
            $table->date('payment_date');
            $table->integer('amount');
            $table->string('bank');
            $table->string('sender_account_name');
            $table->string('notes')->nullable();
            $table->string('receipt_image');
            $table->string('status')->default(\App\Models\Payment::STATUS_WAITING);
            $table->string('reason')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
};
