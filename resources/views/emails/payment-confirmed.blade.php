<table style="font-family:Arial,Helvetica,sans-serif;width:580px;margin:0 auto;border-spacing:0">
    <tbody style="background-color:#efefef">
    <tr>
        <td>
            <div style="text-align:center;padding-top:30px">
                <svg version="1.0" xmlns="http://www.w3.org/2000/svg" width="40.000000pt" height="40.000000pt"
                     viewBox="0 0 40.000000 40.000000" preserveAspectRatio="xMidYMid meet">
                    <g transform="translate(0.000000,40.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
                        <path d="M0 200 l0 -200 200 0 200 0 0 200 0 200 -200 0 -200 0 0 -200z m230
105 l0 -55 38 0 c44 0 76 -20 97 -60 26 -50 21 -57 -46 -64 -45 -5 -59 -10
-51 -18 8 -8 18 -8 36 0 20 9 27 7 40 -7 23 -26 20 -39 -16 -57 -39 -20 -102
-13 -132 15 -15 14 -26 17 -36 11 -22 -14 -93 -12 -113 3 -16 11 -16 16 -4 52
10 28 19 39 31 37 36 -7 46 17 46 109 l0 89 55 0 55 0 0 -55z"></path>
                        <path d="M250 170 c0 -5 7 -10 15 -10 8 0 15 5 15 10 0 6 -7 10 -15 10 -8 0
-15 -4 -15 -10z"></path>
                    </g>
                </svg>
            </div>
            <div style="background-color:white;margin:30px 20px 0px;border:1px solid #ddd;padding:30px;border-radius:10px">
                <h2 style="font-size:28px;margin-bottom:54px;padding:0"> Payment Successfully!!</h2>
                <h3 style="font-size:18px;margin:0;padding:0">Hi, {{ $user->name }}</h3>
                <p style="font-size:16px;margin-bottom:54px;color:#999">We've successfully received your payment.</p>
                <p style="font-size:16px;margin-bottom:42px">Transaction Number: <strong>#{{ $payment->transaction_no }}</strong></p>

                @if ($event->event_type === 'Online')
                    <h3 style="text-align:center;font-size:16px;margin-bottom:0">Your Online Event Link is</h3>
                    <p style="text-align:center;margin-bottom:32px"><a href="{{ $event->location }}">{{ $event->location }}</a></p>
                @else
                    <h3 style="text-align:center;font-size:16px;margin-bottom:0">Your Booking Code is</h3>
                    <h2 style="text-align:center;font-size:32px;color:#f87171;margin-top:10px;">{{ $transaction->booking_code }}</h2>
                @endif

                <table style="border-radius:8px;border:solid 1px #eee;padding:32px 24px;width:100%">
                    <tbody>
                    <tr>
                        <td style="width:50%">Event</td>
                        <td style="text-align:right">
                            <strong>{{ $event->title }}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding:16px 0">
                            <hr style="border:none;border-bottom:1px solid #eee">
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%;padding-left:10px">Price</td>
                        <td style="text-align:right">
                            <strong>{{ rupiah($transaction->price) }}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding:16px 0">
                            <hr style="border:none;border-bottom:1px solid #eee">
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%;padding-left:10px">Quantity</td>
                        <td style="text-align:right">
                            <strong>{{ $transaction->qty }}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding:16px 0">
                            <hr style="border:none;border-bottom:1px solid #eee">
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%">Total Price</td>
                        <td style="text-align:right">
                            <strong>{{ rupiah($transaction->price * $transaction->qty) }}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding:16px 0">
                            <hr style="border:none;border-bottom:1px solid #eee">
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%">Payment Date</td>
                        <td style="text-align:right">
                            <strong>{{ $payment->payment_date->format('d M Y') }}</strong>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding:16px 0">
                            <hr style="border:none;border-bottom:1px solid #eee">
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%">Payment Method</td>
                        <td style="text-align:right">
                            <strong>Transfer Bank <span style="text-transform: uppercase">{{ $payment->bank }}</span></strong>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </td>
    </tr>
    </tbody>
    <tfoot>
    <tr style="background-color:#000;text-align:left;font-size:11px;height:30px;color:#fff">
        <td>
            <div style="text-align:center;margin-bottom:10px;margin-top:15px">
                © Copyright {{ date('Y') }} <a href="#"><span class="il">jeventstudio</span>.com</a>
            </div>
            <p style="margin:0 auto 10px auto;width:400px;text-align:center">
                PT <span class="il">Jevent</span> Studio <br>
                0812-1234-5678 <br>
                Jl Raya Jakarta Bogor Km 28, DKI Jakarta
            </p>
        </td>
    </tr>
    </tfoot>
</table>
