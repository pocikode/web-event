<!-- Modal -->
<div class="modal fade" id="eventBookModal" tabindex="-1" aria-labelledby="eventBookModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl !max-w-[1000px]">
        <div class="modal-content">
            <!-- TODO: hidden form -->
            <form id="transactionForm" action="{{ route('transaction.book') }}" method="post" class="d-none">
                @csrf
                <input type="hidden" name="event_id" value="{{ $event->id }}">
                <input type="hidden" name="qty" value="0">
            </form>

            <div class="modal-body p-0">
                <button type="button" class="btn-close position-absolute z-10" data-bs-dismiss="modal" aria-label="Close"
                        style="top: 5px; right: 5px"
                ></button>
                <div class="row">
                    <div class="col-12 col-lg-7 pr-lg-0">
                        <div class="d-flex flex-column py-3">
                            <div class="text-center border-bottom mt-2">
                                <h2 class="fs-6 fw-bold mb-3">{{ $event->title }}</h2>
                                <p class="fw-normal text-xs mb-2">{{ $event->start_time->format('D, M d, Y, h:i A') }} - {{ $event->end_time->format('D, M d, Y, h:i A T') }}</p>
                            </div>
                            <div class="flex-grow-1 border-bottom">
                                <div class="px-3 py-8 lg:mx-14">
                                    <div class="d-flex justify-content-between align-items-center mb-2">
                                        <span class="fw-normal">Price</span>
                                        <span>{{ rupiah($event->price) }}</span>
                                    </div>
                                    <div class="d-flex justify-content-between align-items-center border-bottom pb-2">
                                        <span class="fw-normal">Quantity</span>
                                        <select class="form-select form-select-sm w-auto" id="selectTicketQuantity" aria-label="Select Quantity">
                                            <option value="0" selected>0</option>
                                            @for ($i = 1; $i <= $event->capacity; $i++)
                                                <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="d-flex justify-content-between mt-1">
                                        <span class="fw-normal">Total</span>
                                        <span id="totalPrice">Rp0</span>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-end px-3 lg:mx-14">
                                <button type="button" id="btnSubmitCheckout" class="btn btn-primary px-5 mt-3" disabled>Checkout</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-5 ps-lg-0 d-none d-lg-block">
                        <img src="{{ asset('uploads/event/' . $event->image) }}" alt="{{ $event->title }}"
                             class="img-fluid min-h-full">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
