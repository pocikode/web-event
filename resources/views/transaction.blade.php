<x-app-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-xl-9">
                <div class="card mt-3">
                    <div class="card-body md:mx-8">
                        <!-- invoice header -->
                        <div class="row justify-content-between">
                            <div class="col-md-6 mb-3">
                                <h3 class="fs-5">
                                    <span class="fw-normal">Invoice No</span>
                                    <span class="fw-bold">#{{ $transaction->transaction_no }}</span>
                                </h3>
                                <h3 class="fs-5">
                                    <span class="fw-normal">Status: </span>
                                    <span class="fw-bold text-danger text-uppercase">{{ str_replace('_', ' ', $transaction->status) }}</span>
                                </h3>
                            </div>
                            <div class="col-md-3 mb-3">
                                <h4 class="fs-5 fw-bold mb-1">Invoiced To</h4>
                                <p class="mb-0 fw-normal text-sm">{{ $transaction->user->name }}</p>
                                <p class="mb-0 fw-normal text-sm">{{ $transaction->user->email }}</p>
                            </div>
                        </div>

                        <!-- invoice detail -->
                        <div class="card mt-3">
                            <div class="card-header bg-white">
                                <h5 class="card-title mb-0">Order Detail</h5>
                            </div>
                            <div class="table-responsive py-3">
                                <table class="table table-borderless table-sm">
                                    <tbody>
                                        <tr>
                                            <th width="30%" class="px-3">Event</th>
                                            <td class="fw-normal">
                                                <a target="_blank" href="{{ route('events.show', $transaction->event->id) }}" class="text-decoration-none">{{ $transaction->event->title }}</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="px-3">Ticket Price</th>
                                            <td class="fw-normal">{{ rupiah($transaction->price) }}</td>
                                        </tr>
                                        <tr>
                                            <th class="px-3">Ticket Quantity</th>
                                            <td class="fw-normal">{{ $transaction->qty }}</td>
                                        </tr>
                                        <tr>
                                            <th class="px-3">Total Price</th>
                                            <td class="fw-bold text-info">{{ rupiah($transaction->price * $transaction->qty) }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <!-- bank list -->
                        <h3 class="mt-5 fs-6">Please choose your preferred method of payment.</h3>
                        <div class="accordion" id="accordionExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <img src="{{ asset('assets/bank/bca-logo.png') }}" alt="Logo Bank BCA" class="w-auto max-h-[25px]">
                                    </button>
                                </h2>
                                <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <p class="fw-normal mb-0">BCA 8612345678 <br>
                                            a/n PT Jakarta Event <br>
                                            KCP Jakarta Pusat, DKI Jakarta
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <img src="{{ asset('assets/bank/mandiri-logo.png') }}" alt="Logo Bank Mandiri" class="w-auto max-h-[30px]">
                                    </button>
                                </h2>
                                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <p class="fw-normal mb-0">Mandiri 13700123456776 <br>
                                            a/n PT Jakarta Event <br>
                                            KCP Jakarta Pusat, DKI Jakarta
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingThree">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <img src="{{ asset('assets/bank/bni-logo.png') }}" alt="Logo Bank BNI" class="w-auto max-h-[20px]">
                                    </button>
                                </h2>
                                <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <p class="fw-normal mb-0">BNI 123456112341232 <br>
                                            a/n PT Jakarta Event <br>
                                            KCP Jakarta Pusat, DKI Jakarta
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="text-center mt-3">
                            <a href="{{ route('payment.create') }}" class="btn btn-success">Click Here to Confirm Your Payment</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
