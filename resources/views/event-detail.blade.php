<x-app-layout>
    <div class="container pt-4 pb-3">
        <div class="card">
            <div class="card-body p-0">
                <!-- Event Detail Header -->
                <div class="max-h-[360px]">
                    <div class="row border-b">
                        <div class="col-12 col-md-8 max-h-[360px] mw-100">
                            <img src="{{ asset('uploads/event/' . $event->image) }}" alt="{{ $event->title }}"
                                 class="mh-100 w-100">
                        </div>
                        <div class="col-12 col-md-4 px-3 py-4 max-h-[360px]">
                            <div class="position-relative w-100 h-100">
                                <span class="fw-light text-sm">{{ $event->start_time->format('M d') }}</span>
                                <h1 class="mt-5 fs-5 fw-bold">{{ $event->title }}</h1>
                                <p class="fw-light text-sm">by <span class="text-info font-normal">{{ $event->event_organizer }}</span></p>
                                <div class="w-full position-absolute bottom-0 pe-3">
                                    <p class="font-normal mb-1">{{ rupiah($event->price) }}</p>
                                    <div class="d-grid gap-2">
                                        <button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#eventBookModal">
                                            Tickets
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row px-3 pt-8 pb-4">
                    <div class="col-12 col-md-8 mt-3">
                        <div class="mx-md-5">
                            <p class="mb-5">{{ $event->event_summ }}</p>
                            <h3 class="fs-5 fw-bold mb-3">About this event</h3>
                            {!! $event->event_desc !!}
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="mb-5 mt-3">
                            <h3 class="fs-5">Date and time</h3>
                            <div class="fw-light">
                                <p class="mb-0">{{ $event->start_time->format('D, M d, Y, h:i A') }} -</p>
                                <p class="mb-0">{{ $event->end_time->format('D, M d, Y, h:i A T') }}</p>
                            </div>
                        </div>
                        <div class="mb-3">
                            <h3 class="fs-5">Location</h3>
                            <div class="font-semibold">
                                <p>{{ $event->event_type === 'Online' ? 'Online Event' : $event->location }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('event-book-modal')

    @push('scripts')
        <script>
            const transactionForm = $('#transactionForm')

            $(document).ready(function () {
                $('#selectTicketQuantity').change(function () {
                    $('#totalPrice').text(rupiah(parseInt(this.value) * parseInt('{{ $event->price }}')))
                    $('#btnSubmitCheckout').prop('disabled', this.value == 0)

                    transactionForm.find('[name="qty"]').val(this.value)
                })

                $('#btnSubmitCheckout').click(() => transactionForm.submit())
            })

            function rupiah(number) {
                return new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR',
                    trailingZeroDisplay: 'stripIfInteger'
                }).format(number).replace(/(\.|,)00$/g, '')
            }
        </script>
    @endpush
</x-app-layout>
