<x-app-layout>
    <div class="row justify-content-center">
        <div class="col-sm-6">
            <div class="card mt-3">
                <div class="card-body">
                    @if(session('success'))
                        <div class="alert alert-success alert-dismissible fade show mb-4" role="alert">
                            <div class="flex align-items-center">
                                <i class="fa fa-check-circle fa-2x" aria-hidden="true"></i>
                                <span class="ms-2 fw-normal">Payment confirmation created successfully</span>
                            </div>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif

                    <form action="{{ route('payment.confirm') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row mb-3">
                            <div class="col-sm-4 col-xs-12 text-right">
                                <label for="inputName" class="col-form-label fw-normal">Name</label>
                            </div>
                            <div class="col-sm-8 col-xs-12">
                                <input type="text" name="name" id="inputName" class="form-control" aria-describedby="inputNameHelpline" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-4 col-xs-12 text-right">
                                <label for="inputEmail" class="col-form-label fw-normal">Email</label>
                            </div>
                            <div class="col-sm-8 col-xs-12">
                                <input type="email" name="email" id="inputEmail" class="form-control" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-4 col-xs-12 text-right">
                                <label for="inputPaymentDate" class="col-form-label fw-normal">Payment Date</label>
                            </div>
                            <div class="col-sm-8 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
                                    <input type="text" name="payment_date" id="inputPaymentDate" class="form-control" aria-describedby="basic-addon1" required>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-4 col-xs-12 text-right">
                                <label for="inputPaymentAmount" class="col-form-label fw-normal">Payment Amount</label>
                            </div>
                            <div class="col-sm-8 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-text" id="basic-addon2">Rp</span>
                                    <input type="text" name="payment_amount" id="inputPaymentAmount" class="form-control" aria-describedby="basic-addon2" placeholder="99.500" required>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-4 col-xs-12 text-right">
                                <label for="inputBankDestination" class="col-form-label fw-normal">Bank Destination</label>
                            </div>
                            <div class="col-sm-8 col-xs-12">
                                <select name="bank" id="inputBankDestination" class="form-select">
                                    <option value="bca">BCA</option>
                                    <option value="bni">BNI</option>
                                    <option value="mandiri">Mandiri</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-4 col-xs-12 text-right">
                                <label for="inputSenderName" class="col-form-label fw-normal">Sender's Account Name</label>
                            </div>
                            <div class="col-sm-8 col-xs-12">
                                <input type="text" name="sender_account_name" id="inputSenderName" class="form-control" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-4 col-xs-12 text-right">
                                <label for="inputTransactionNumber" class="col-form-label fw-normal">Transaction Number</label>
                            </div>
                            <div class="col-sm-8 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-text" id="basic-addon3">#</span>
                                    <input type="text" name="transaction_no" id="inputTransactionNumber" class="form-control" aria-describedby="basic-addon3" placeholder="202205" required>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-4 col-xs-12 text-right">
                                <label for="inputNotes" class="col-form-label fw-normal">Notes</label>
                            </div>
                            <div class="col-sm-8 col-xs-12">
                                <textarea class="form-control" name="notes" id="inputNotes" rows="2" placeholder="optional"></textarea>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-sm-4 col-xs-12 text-right">
                                <label for="inputPaymentReceipt" class="col-form-label fw-normal">Payment Receipt</label>
                            </div>
                            <div class="col-sm-8 col-xs-12">
                                <input id="inputPaymentReceipt" type="file" name="payment_receipt" class="form-control" accept="image/*" required>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-success !px-16">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            $(document).ready(function () {
                $('#inputPaymentDate').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    minYear: 2022,
                    maxYear: parseInt(moment().format('YYYY'),10)
                })
            })
        </script>
    @endpush
</x-app-layout>
