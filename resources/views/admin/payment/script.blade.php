<script>
    const paymentContainer = $('#paymentContainer')
    const formSubmitApproval = $('#formSubmitApproval')
    const detailModal = new bootstrap.Modal(document.getElementById('detailPaymentModal'))

    $(document).ready(function () {
        $('.btn-detail-payment').click(function (event) {
            $('.fillable-detail').empty()

            const button    = $(event.currentTarget)
            const paymentId = button.data('payment_id')

            paymentContainer.LoadingOverlay('show')

            axios.get('{{ route('dashboard.payment.index') }}/' + paymentId)
                .then(res => {
                    $('#detail-image-receipt').append(`<img src="${res.data.receipt_image_url}" class="img-fluid">`)
                    $('#detail-transaction-no').text('#' + res.data.transaction_no)
                    $('#detail-name').text(res.data.name)
                    $('#detail-email').text(res.data.email)
                    $('#detail-payment-date').text(res.data.payment_date)
                    $('#detail-amount').text(rupiah(res.data.amount))
                    $('#detail-bank').text(res.data.bank)
                    $('#detail-sender-account-name').text(res.data.sender_account_name)
                    $('#detail-notes').text(res.data.notes)
                    $('#detail-status').text(res.data.status)

                    $('.row-status-waiting').toggleClass('d-none', res.data.status !== 'waiting')
                    $('.row-status-not-waiting').toggleClass('d-none', res.data.status === 'waiting')

                    formSubmitApproval.prop('action', '{{ route('dashboard.payment.index') }}/' + res.data.id)

                    detailModal.show()
                })
                .catch(err => console.log(err))
                .finally(() => paymentContainer.LoadingOverlay('hide'))
        })

        $('.inline-radio-status').change(function () {
            const reasonEl = $('#row-detail-reason-reject-input')

            if (this.value === 'approved') {
                reasonEl.addClass('d-none')
            } else {
                reasonEl.removeClass('d-none')
            }
        })

        $('#btnSubmitApproval').click(function () {
            const status = $('.inline-radio-status:checked').val()

            if (status) {
                formSubmitApproval.find('input[name="status"]').val(status)
                formSubmitApproval.submit()
            }
        })
    })

    function rupiah(number) {
        return new Intl.NumberFormat('id-ID', {
            style: 'currency',
            currency: 'IDR',
            trailingZeroDisplay: 'stripIfInteger'
        }).format(number).replace(/(\.|,)00$/g, '')
    }
</script>
