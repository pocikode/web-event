<x-app-layout>
    <div id="paymentContainer" class="container my-5">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title fs-4 mb-0">Payment</h3>
            </div>
            <div class="card-body">
                @if (session('error'))
                    <div class="mb-3">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>Error:</strong> <span class="fw-normal">{{ session('error') }}</span>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    </div>
                @endif

                @if (session('success'))
                    <div class="mb-3">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Success:</strong> <span class="fw-normal">{{ session('success') }}</span>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    </div>
                @endif

                <div class="table-responsive">
                    @if ($payments->isEmpty())
                        <h4 class="text-center text-base">Data Empty</h4>
                    @else
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Transaction No</th>
                                <th>Amount</th>
                                <th>Bank Destination</th>
                                <th>Sender Account Name</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($payments as $payment)
                                <tr>
                                    <td>{{ $payment->transaction_no }}</td>
                                    <td>{{ rupiah($payment->amount) }}</td>
                                    <td>{{ strtoupper($payment->bank) }}</td>
                                    <td>{{ $payment->sender_account_name }}</td>
                                    <td class="text-center {{ $statusColors[$payment->status] }}">
                                        {{ ucwords($payment->status) }}
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-outline btn-outline-success btn-detail-payment"
                                                data-payment_id="{{ $payment->id }}"
                                        >Detail</button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <!-- Pagination -->
                        {{ $payments->links() }}
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!-- form submit approval -->
    <form id="formSubmitApproval" action="" class="d-none" method="post">
        @csrf
        @method('put')
        <input type="hidden" name="status">
    </form>

    @include('admin.payment.detail-modal')

    @push('scripts')
        @include('admin.payment.script')
    @endpush
</x-app-layout>
