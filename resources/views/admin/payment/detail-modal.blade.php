<div class="modal fade" id="detailPaymentModal" tabindex="-1" aria-labelledby="detailPaymentModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detailPaymentModalLabel">Payment Detail</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-base">
                <div class="row">
                    <div class="col-md-6">
                        <div id="detail-image-receipt" class="fillable-detail max-h-[360px] overflow-y-scroll"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="table-responsive">
                            <table class="table table-sm table-borderless">
                                <tbody>
                                <tr>
                                    <th class="fw-normal" style="width: 180px">Transaction Number</th>
                                    <td style="width: 5px">:</td>
                                    <td class="fillable-detail" id="detail-transaction-no"></td>
                                </tr>
                                <tr>
                                    <th class="fw-normal">Name</th>
                                    <td style="width: 5px">:</td>
                                    <td id="detail-name" class="fillable-detail"></td>
                                </tr>
                                <tr>
                                    <th class="fw-normal">Email</th>
                                    <td style="width: 5px">:</td>
                                    <td id="detail-email" class="fillable-detail"></td>
                                </tr>
                                <tr>
                                    <th class="fw-normal">Payment Date</th>
                                    <td style="width: 5px">:</td>
                                    <td id="detail-payment-date" class="fillable-detail"></td>
                                </tr>
                                <tr>
                                    <th class="fw-normal">Amount</th>
                                    <td style="width: 5px">:</td>
                                    <td id="detail-amount" class="fillable-detail"></td>
                                </tr>
                                <tr>
                                    <th class="fw-normal">Bank</th>
                                    <td style="width: 5px">:</td>
                                    <td id="detail-bank" class="fillable-detail"></td>
                                </tr>
                                <tr>
                                    <th class="fw-normal">Sender Account Name</th>
                                    <td style="width: 5px">:</td>
                                    <td id="detail-sender-account-name" class="fillable-detail"></td>
                                </tr>
                                <tr>
                                    <th class="fw-normal">Notes</th>
                                    <td style="width: 5px">:</td>
                                    <td id="detail-notes" class="fillable-detail"></td>
                                </tr>
                                <tr class="row-status-not-waiting">
                                    <th class="fw-normal">Status</th>
                                    <td style="width: 5px">:</td>
                                    <td id="detail-status" class="fillable-detail"></td>
                                </tr>
                                </tbody>
                                <tbody class="row-status-waiting">
                                <tr>
                                    <th class="fw-normal">Status</th>
                                    <td style="width: 5px">:</td>
                                    <td id="detail-status">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input inline-radio-status" type="radio" name="status" id="inlineRadio1" value="approved">
                                            <label class="form-check-label" for="inlineRadio1">Approve</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input inline-radio-status checked:!bg-red-500" type="radio" name="status" id="inlineRadio2" value="rejected">
                                            <label class="form-check-label" for="inlineRadio2">Reject</label>
                                        </div>
                                    </td>
                                </tr>
                                {{--                            <tr id="row-detail-reason-reject-input" class="d-none">--}}
                                {{--                                <td class="fw-normal">Reason</td>--}}
                                {{--                                <td>:</td>--}}
                                {{--                                <td>--}}
                                {{--                                    <textarea class="form-control" name="reason" id="detail-reason-reject-input" rows="2" placeholder="type here" required></textarea>--}}
                                {{--                                    <div class="invalid-feedback" id="detail--approval-reason-validation"></div>--}}
                                {{--                                </td>--}}
                                {{--                            </tr>--}}
                                </tbody>
                            </table>
                        </div>

                        <div class="text-center">
                            <button type="button" id="btnSubmitApproval" class="btn btn-primary fw-bol !px-12">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline btn-outline-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
